\contentsline {lstlisting}{\numberline {1.1}Le manifeste : manifest.manifest}{3}
\contentsline {lstlisting}{\numberline {2.1}L'option draft}{8}
\contentsline {lstlisting}{\numberline {3.1}L'environnement de citation}{11}
\contentsline {lstlisting}{\numberline {3.2}Les commandes de placement des figures}{12}
\contentsline {lstlisting}{\numberline {3.3}La commande de r\IeC {\'e}f\IeC {\'e}rence \IeC {\`a} une figure}{12}
\contentsline {lstlisting}{\numberline {3.4}Commande classique de placement d'une figure}{12}
\contentsline {lstlisting}{\numberline {3.5}Commande de placement de multiples figures}{13}
\contentsline {lstlisting}{\numberline {3.6}L'environnement pour placer un tableau}{14}
\contentsline {lstlisting}{\numberline {3.7}Changer l'intitul\IeC {\'e} de la r\IeC {\'e}f\IeC {\'e}rence}{14}
\contentsline {lstlisting}{\numberline {3.8}L'environnement pour importer un tableau Gnumeric}{15}
\contentsline {lstlisting}{\numberline {3.9}Un tableau plus complexe}{16}
\contentsline {lstlisting}{\numberline {3.10}L'environnement pour placer du code}{18}
\contentsline {lstlisting}{\numberline {3.11}La r\IeC {\'e}f\IeC {\'e}rence \IeC {\`a} un site web}{18}
\contentsline {lstlisting}{\numberline {3.12}La r\IeC {\'e}f\IeC {\'e}rence \IeC {\`a} un ouvrage}{20}
